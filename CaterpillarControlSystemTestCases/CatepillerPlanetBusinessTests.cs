﻿using CaterpillarControlSystem;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CaterpillarControlSystemTestCases
{
    [TestFixture]
    public class CatepillerPlanetBusinessTests
    {
        [Test]
        public void Ctor_CatepillerSizeLessThen5_Throws()
        {
            Assert.Throws<ArgumentException>(
                () => new Planet(1, Direction.Right));
        }

        [Test]
        public void Ctor_ValidParams_PlanetIsInCorrectState()
        {
            var catepillarSize = 3;
            var direction = Direction.Right;

            Planet sut = new(catepillarSize, direction);

            Assert.That(sut.CatepillarSize, Is.EqualTo(catepillarSize));
            Assert.That(sut.Direction, Is.EqualTo(direction));
        }


        [Test]
        [TestCase(Direction.Left, Direction.Left)]
        [TestCase(Direction.Left, Direction.Right)]
        [TestCase(Direction.Right, Direction.Right)]
        [TestCase(Direction.Right, Direction.Left)]
        [TestCase(Direction.Up, Direction.Up)]
        [TestCase(Direction.Up, Direction.Down)]
        [TestCase(Direction.Down, Direction.Down)]
        [TestCase(Direction.Down, Direction.Up)]
        public void CatepillerMakesMove_InvalidMovement_ReturnsFalse(
      Direction catepillerDirection, Direction CatepillerDirection)
        {
            Planet sut = new(3, catepillerDirection);
            Assert.That(sut.CanMove(CatepillerDirection), Is.EqualTo(false));
        }

        [Test]
        [TestCase(Direction.Left, Direction.Up)]
        [TestCase(Direction.Left, Direction.Down)]
        [TestCase(Direction.Right, Direction.Up)]
        [TestCase(Direction.Right, Direction.Down)]
        [TestCase(Direction.Up, Direction.Left)]
        [TestCase(Direction.Up, Direction.Right)]
        [TestCase(Direction.Down, Direction.Left)]
        [TestCase(Direction.Down, Direction.Right)]
        public void CatepillerMakesMove_ValidMovement_ReturnsTrue(
            Direction catepillerDirection, Direction CatepillerDirection)
        {
            Planet sut = new(3, catepillerDirection);
            Assert.That(sut.CanMove(CatepillerDirection), Is.EqualTo(true));
        }


        [Test]
        [TestCase(1, Direction.Right, 4, 0)]
        [TestCase(2, Direction.Right, 5, 0)]
        [TestCase(3, Direction.Right, 6, 0)]
        [TestCase(1, Direction.Down, 3, 1)]
        [TestCase(2, Direction.Down, 3, 2)]
        [TestCase(3, Direction.Down, 3, 3)]
        [TestCase(1, Direction.Up, 3, -1)]
        [TestCase(2, Direction.Up, 3, -2)]
        [TestCase(3, Direction.Up, 3, -3)]
        public void PlanetUpdate_NTimes_CatepillerPosIncNStep(int times, Direction direction, int posX, int posY)
        {
            Planet sut = new(3, direction);
            for (var i = 0; i < times; i++) sut.Update();
            Assert.That(sut.CatepillarHead, Is.EqualTo(new ConsolePoint(posX, posY)));
        }

        [Test]
        public void PlanetUpdate_CatepillerMove_CatepillerTailChanged()
        {
            Planet sut = new(3, Direction.Right);
            var tail = sut.CatepillarTail;
            sut.Update();
            Assert.That(sut.CatepillarTail, Is.Not.EqualTo(tail));
        }

        [Test]
        [TestCase(3, Direction.Right, 4, 0)]
        [TestCase(3, Direction.Down, 3, 1)]
        [TestCase(3, Direction.Up, 3, -1)]
        public void PlanetUpdate_CatepillerTakeBonus_CatepillerSizeIncrement(
      int catepillarSize,
      Direction direction,
      int bonusX,
      int bonusY)
        {
            var expectedCatepillerSize = catepillarSize + 1;

            var gen = new TestPointGenerator(bonusX, bonusY);
            Planet sut = new(catepillarSize, direction, gen);
            sut.Update();

            Assert.That(sut.CatepillarSize, Is.EqualTo(expectedCatepillerSize));
        }


        [Test]
        [TestCase(3, 3, 3, Direction.Down, 3)]
        [TestCase(3, 3, -3, Direction.Up, 3)]
        public void CatepillerMakesMove_CatepillerTakeBonus_CatepillerSizeIncrement(int catepillarSize, int bonusX, int bonusY, Direction movement, int updateTimes)
        {
            var bonusAte = false;
            var expectedCatepillerSize = catepillarSize + 1;

            var gen = new TestPointGenerator(bonusX, bonusY);
            Planet sut = new(catepillarSize, Direction.Right, gen);

            sut.BonusTake += (sender, pos) => bonusAte = true;

            //sut.Move(movement);


            for (var i = 0; i < updateTimes; i++)
                sut.Update();

            Assert.That(sut.CatepillarSize, Is.EqualTo(expectedCatepillerSize));
            Assert.That(bonusAte, Is.EqualTo(true));
            Assert.That(sut.Score, Is.EqualTo(1));
        }


    }
}
