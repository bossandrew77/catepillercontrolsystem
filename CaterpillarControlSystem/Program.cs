﻿using System;
using System.Threading.Tasks;
using CaterpillarControlSystem;
//using CatepillerPlanet.Business;

object padlock = new object();
Planet _catepillar;
string part = "$*********$**********$********\r\n***$*******B*************#****\r\n************************#*****\r\n***#**************************\r\n**$*************************#*\r\n$$***#************************\r\n**************$***************\r\n**********$*********$*****#***\r\n********************$*******$*\r\n*********#****$***************\r\n**B*********$*****************\r\n*************$$****B**********\r\n****$************************B\r\n**********************#*******\r\n***********************$***B**\r\n********$***$*****************\r\n************$*****************\r\n*********$********************\r\n*********************#********\r\n*******$**********************\r\n*#***$****************#*******\r\n****#****$****$********B******\r\n***#**$********************$**\r\n***************#**************\r\n***********$******************\r\n****B****#******B*************\r\n***$***************$*****B****\r\n**********$*********#*$*******\r\n**************#********B******\r\ns**********$*********#*B******\r\n";
    InitPlanet();
InitConsole();

var keyboardTask = KeyboardListener();
var updateTask = Update();

Task.WaitAll(keyboardTask, updateTask);

void InitConsole()
{
    Console.CursorVisible = false;
    Console.Clear();
    Console.WriteLine(part);
    //ShowCatepillerBonus();
    _catepillar.Catepillar.ForEach(s
        => WritePos("H", s));
}

void InitPlanet()
{
    _catepillar = new(3, Direction.Right);
    _catepillar.PlanetOver += OnNewSpaceArea;
    _catepillar.BonusTake += OnBonusTake;
}

void OnBonusTake(object sender, ConsolePoint e)
{
    ShowCatepillerBonus();
}

void OnNewSpaceArea(object sender, ConsolePoint e)
{
    WritePos("Next Planet Space", ConsolePoint.TopLeftCorner);
}





async Task KeyboardListener()
{


    while (true)
    {
        if (Console.KeyAvailable)
        {
          


            var key = Console.ReadKey(intercept: true).Key;


       
            if (key == ConsoleKey.Enter)
            {
                Console.SetCursorPosition(50, 31);
                Console.WriteLine("\nPress ENTER Next Steps to ...");
             var text=   Console.ReadLine();
            }








                lock (padlock)
            {

              var direction= _catepillar.OnKeyPress(key);

                WritePos("*", _catepillar.CatepillarTail);
               // _catepillar.Move(direction.Value);
                _catepillar.Move(direction);
                _catepillar.Update();
                WritePos("H", _catepillar.CatepillarHead);
            }
        }
        await Task.Delay(1);
    }
}
async Task Update()
{
 
    Console.WriteLine();
}

void ShowCatepillerBonus()
{
    WritePos("S", _catepillar.CatepillerPos);
    WritePos($"Bonus: {_catepillar.Score}", ConsolePoint.BottomLeftCorner);
}



void WritePos(string message, ConsolePoint point)
{
    (Console.CursorLeft, Console.CursorTop) = point;
    Console.Write(message);
}


// void OnKeyPress(ConsoleKey key)
//{


//    Direction newDirection;

//    switch (key)
//    {
//        case ConsoleKey.W:
//            newDirection = Direction.Up;
//            break;

//        case ConsoleKey.A:
//            newDirection = Direction.Left;
//            break;

//        case ConsoleKey.S:
//            newDirection = Direction.Down;
//            break;

//        case ConsoleKey.D:
//            newDirection = Direction.Right;
//            break;

//        default:
//            return;
//    }

//    // Catepillar cannot turn 180 degrees.
//    if (newDirection == OppositeDirectionTo(_currentDirection))
//    {
//        return;
//    }

//    _nextDirection = newDirection;
//}
 static Direction OppositeDirectionTo(Direction direction)
{
    switch (direction)
    {
        case Direction.Up: return Direction.Down;
        case Direction.Left: return Direction.Right;
        case Direction.Right: return Direction.Left;
        case Direction.Down: return Direction.Up;
        default: throw new ArgumentOutOfRangeException();
    }
}