﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace CaterpillarControlSystem
{
    public class Planet 
    {
      
        public Direction Direction { get; private set; }


        public List<ConsolePoint> Catepillar { get; private set; } = new();
  
        public ConsolePoint CatepillarHead => Catepillar.FirstOrDefault();
        public ConsolePoint CatepillarTail => Catepillar.LastOrDefault();
        public int CatepillarSize => Catepillar.Count;


        private readonly int _initialSize;
        public int Score => CatepillarSize - _initialSize;
        public event EventHandler<ConsolePoint> BonusTake;

        public bool IsPlanetOver { get; set; }
        public event EventHandler<ConsolePoint> PlanetOver;


        private Direction _currentDirection;
        private Direction _nextDirection;


        public Planet(int catepillarSize, Direction direction, IRandomPointGenerator randomPointGenerator)
        {
            if (catepillarSize < 5)
                throw new ArgumentException("Catepiller size has to be >= 5");

            _initialSize = catepillarSize;
            Direction = direction;
            _randomPointGenerator = randomPointGenerator;
            InitCatepillar(catepillarSize);
            InitBonus();

            _currentDirection = Direction.Right;
            _nextDirection = Direction.Right;
        }

        private bool CheckBonusBooster(ConsolePoint newCatepillarPos)
        {
            if (newCatepillarPos != CatepillerPos)
                return false;

            InitBonus();
            Catepillar.Insert(0, newCatepillarPos);
            BonusTake?.Invoke(this, newCatepillarPos);
            return true;
        }



      


        private readonly IRandomPointGenerator _randomPointGenerator;
        public ConsolePoint CatepillerPos { get; private set; }

       
        private void InitBonus()
        {
            CatepillerPos = _randomPointGenerator.Next(Catepillar);
        }

        public bool CanMove(Direction direction)
        {
            return (Direction, direction) switch
            {
                (Direction.Left, Direction.Left) => false,
                (Direction.Left, Direction.Right) => false,
                (Direction.Right, Direction.Right) => false,
                (Direction.Right, Direction.Left) => false,
                (Direction.Up, Direction.Up) => false,
                (Direction.Up, Direction.Down) => false,
                (Direction.Down, Direction.Down) => false,
                (Direction.Down, Direction.Up) => false,
                _ => true
            };
        }

        public Planet(int catepillarSize, Direction direction)
        {
            if (catepillarSize < 3)
                throw new ArgumentException("Catepillar size has to be >= 5");

            Direction = direction;
            InitCatepillar(catepillarSize);
        }

        public void Update()
        {
            if (IsPlanetOver)
                return;

            var newCatepillarPos = CatepillarHead.Move(Direction);

            if (CheckPlanetOver(newCatepillarPos))
                return;

            if (!CheckBonusBooster(newCatepillarPos))
                MoveCatepiller(newCatepillarPos);
        }



        public Direction OnKeyPress(ConsoleKey key)
        {
            Direction newDirection;

            switch (key)
            {
                case ConsoleKey.W:
                    newDirection = Direction.Up;
                    break;

                case ConsoleKey.A:
                    newDirection = Direction.Left;
                    break;

                case ConsoleKey.S:
                    newDirection = Direction.Down;
                    break;

                case ConsoleKey.D:
                    newDirection = Direction.Right;
                    break;

                default:
                    newDirection = Direction.Right;
                    break;
            }

            // Catepillar cannot turn 180 degrees.
            if (newDirection == OppositeDirectionTo(_currentDirection))
            {
               // return;
            }

            _nextDirection=Direction = newDirection;

            return newDirection;
        }

        private bool CheckPlanetOver(ConsolePoint newCatepillarPos)
        {
            if (IsPlanetOver)
                return true;

            if (!Catepillar.Contains(newCatepillarPos))
                return false;

            IsPlanetOver = true;
            PlanetOver?.Invoke(this, newCatepillarPos);

            return true;
        }

        private void InitCatepillar(int catepillarSize)
        {
            ConsolePoint pos = new(catepillarSize, 0);
           // var dir = DirectionUtility.Reverse(Direction);
            var dir = OppositeDirectionTo(Direction);
            for (var i = 0; i < catepillarSize; i++)
            {
                Catepillar.Add(pos);
                pos = pos.Move(dir);
            }
        }

   

        private static Direction OppositeDirectionTo(Direction direction)
        {
            switch (direction)
            {
                case Direction.Up: return Direction.Down;
                case Direction.Left: return Direction.Right;
                case Direction.Right: return Direction.Left;
                case Direction.Down: return Direction.Up;
                default: throw new ArgumentOutOfRangeException();
            }
        }

        //public void Update()
        //{
        //    var newCatepillarPos = CatepillarHead.Move(Direction);
        //    MoveCatepiller(newCatepillarPos);
        //}

        private void MoveCatepiller(ConsolePoint newCatepillarPos)
        {
            Catepillar.Insert(0, newCatepillarPos);
            Catepillar.RemoveAt(Catepillar.Count - 1);
        }

        public void Move(Direction direction)
        {

            CatepillarHead.Move(direction);     
        }



    }

    public class TestPointGenerator : IRandomPointGenerator
    {
        private ConsolePoint _consolePoint;

        public TestPointGenerator(int x, int y)
            => _consolePoint = new(x, y);

        public ConsolePoint Next(List<ConsolePoint> exceptedList)
            => _consolePoint;
    }

    public interface IRandomPointGenerator
    {
        ConsolePoint Next(List<ConsolePoint> exceptedList);
    }
    
    public class RandomPointGenerator : IRandomPointGenerator
    {
        public int ConsoleMaxHeight;
        public int ConsoleMaxWith;

        public ConsolePoint Next(List<ConsolePoint> exceptedList)
        {
            Random rnd = new();
            for (var i = 0; i < ConsoleMaxHeight * ConsoleMaxWith; i++)
            {
                var x = rnd.Next(ConsoleMaxWith);
                var y = rnd.Next(ConsoleMaxHeight);
                ConsolePoint cp = new(x, y);
                if (!exceptedList.Contains(cp))
                    return new(x, y);
            }
            return null;
        }
    }




    public enum Direction
    {
        Up,
        Down,
        Left,
        Right
    }


}
