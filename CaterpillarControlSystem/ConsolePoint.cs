﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CaterpillarControlSystem
{
    public record ConsolePoint
    {
        public const int ConsoleMaxWith = 120;
        public const int ConsoleMaxHeight = 29;

        public static readonly ConsolePoint TopLeftCorner = new() { X = 0, Y = 0 };
        public static readonly ConsolePoint TopRightCorner = new() { X = ConsoleMaxWith, Y = 0 };
        public static readonly ConsolePoint BottomLeftCorner = new() { X = 0, Y = ConsoleMaxHeight };
        public static readonly ConsolePoint BottomRightCorner = new() { X = ConsoleMaxWith, Y = ConsoleMaxHeight };

        public int X { get; init; }
        public int Y { get; init; }

        public ConsolePoint() { }


        public ConsolePoint(int x, int y)
            => (X, Y) = (ArrangeX(x), ArrangeY(y));

        public void Deconstruct(out int x, out int y)
            => (x, y) = (X, Y);

        private ConsolePoint AddX(int value)
            => this with { X = ArrangeX(X + value) };

        private ConsolePoint AddY(int value)
            => this with { Y = ArrangeY(Y + value) };

        public ConsolePoint Move(
            Direction direction)
        {
            return direction switch
            {
                Direction.Right => AddX(1),
                Direction.Left => AddX(-1),
                Direction.Up => AddY(-1),
                Direction.Down => AddY(1),
                _ => throw new ArgumentOutOfRangeException(nameof(direction), direction, null)

            };
        }

        private static int ArrangeX(int x)
        {
            while (x < 0)
                x += ConsoleMaxWith;
            return x % ConsoleMaxWith;
        }

        private static int ArrangeY(int y)
        {
            while (y < 0)
                y += ConsoleMaxHeight;
            return y % ConsoleMaxHeight;
        }
    }
}
